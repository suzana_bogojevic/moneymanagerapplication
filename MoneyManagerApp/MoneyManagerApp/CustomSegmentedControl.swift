//
//  CustomSegmentedControl.swift
//  MoneyManagerApp
//
//  Created by Mini on 1/22/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import UIKit

@IBDesignable
class CustomSegmentedControl: UIControl {
    var buttons = [UIButton]()
    var selector: UIView!
    var selectedSegmentIndex = 0
    var borderWidth = 0.5
    var borderColor = #colorLiteral(red: 0.7506220879, green: 0.7506220879, blue: 0.7506220879, alpha: 1)
    
//    var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    
//    var borderColor: UIColor = UIColor.lightGray {
//        didSet{
//            layer.borderColor = borderColor.cgColor
//        }
//    }
    
    @IBInspectable
    var textColor: UIColor = .white {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable
    var selectorTextColor : UIColor = .white {
        didSet {
            updateView()
        }
    }
    
    func updateView(){
        buttons.removeAll()
        subviews.forEach { $0.removeFromSuperview() }
        let buttonTitles = ["INCOME", "EXPENSE"]
        layer.borderColor = #colorLiteral(red: 0.7506220879, green: 0.7506220879, blue: 0.7506220879, alpha: 1)
        layer.borderWidth = 0.5
        
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitle(buttonTitle, for: .normal)
            button.setTitleColor(textColor, for: .normal)
            button.addTarget(self, action: #selector(buttonTapped(button:)), for: .touchUpInside)
            button.titleLabel!.font = UIFont(name: "Helvetica Neue", size: 13)
            buttons.append(button)
        }
        
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
        
        let selectorWidth = frame.width / CGFloat(buttonTitles.count)
        selector = UIView(frame: CGRect(x: 0, y: 0, width: Int(selectorWidth), height: Int(frame.height)))
        selector.layer.cornerRadius = frame.height/2
        selector.backgroundColor = #colorLiteral(red: 0, green: 0.9079033732, blue: 0.7564690113, alpha: 1)
        addSubview(selector)
        
        let stackView = UIStackView(arrangedSubviews: buttons)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
    }
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = frame.height/2
    }
    
    @objc func buttonTapped(button: UIButton) {
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            
            if btn == button {
                selectedSegmentIndex = buttonIndex
                let selectorStartPosition = frame.width/CGFloat(buttons.count) * CGFloat(buttonIndex)
                UIView.animate(withDuration: 0.4, animations: {
                    self.selector.frame.origin.x = selectorStartPosition
                })
                btn.setTitleColor(selectorTextColor, for: .normal)
                selector.backgroundColor = #colorLiteral(red: 0.8900626302, green: 0.01348789688, blue: 0, alpha: 1)
            }else {
                selector.backgroundColor = #colorLiteral(red: 0, green: 0.9079033732, blue: 0.7564690113, alpha: 1)
            }
        }
        sendActions(for: .valueChanged)
    }
}

//
//  DataView.swift
//  MoneyManagerApp
//
//  Created by Mini on 1/17/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import UIKit

@IBDesignable
class DataView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    
}


//
//  NotificationNameExtension.swift
//  MoneyManagerApp
//
//  Created by Mini on 1/31/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import Foundation

extension Double {
    
    /// Formats the receiver as a currency string using the specified three digit currencyCode. Currency codes are based on the ISO 4217 standard.
    func formatAsCurrency(currencyCode: String) -> String? {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        currencyFormatter.currencyCode = currencyCode
        currencyFormatter.maximumFractionDigits = floor(self) == self ? 0 : 2
        return currencyFormatter.string(from: self as NSNumber)
    }
}


//
//  UIButtonBorder.swift
//  MoneyManagerApp
//
//  Created by Mini on 1/17/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import Foundation
import UIKit

public enum UIButtonBorderSide {
    case Top, Bottom, LeftTop, LeftBottom, Right
}

extension UIButton {
    
    public func addBorder(side: UIButtonBorderSide, color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        switch side {
        case .Top:
            border.frame = CGRect(x: 0, y: 0, width: bounds.size.width , height: width  )
        case .Bottom:
            border.frame = CGRect(x: 5, y: self.bounds.size.height - width, width: self.bounds.size.width*0.87, height: width)
        case .LeftTop:
            border.frame = CGRect(x: 0, y: 5, width: width, height: self.bounds.size.height)
        case .LeftBottom:
            border.frame = CGRect(x: 0, y: 0, width: width, height: self.bounds.size.height*0.97)
        case .Right:
            border.frame = CGRect(x: self.bounds.size.width - width, y: 5, width: width, height: self.bounds.size.height*0.82)
        }
        
        self.addSubview(border)
    }
    
}

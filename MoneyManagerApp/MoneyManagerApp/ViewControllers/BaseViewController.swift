//
//  BaseViewController.swift
//  MoneyManagerApp
//
//  Created by Mini on 2/6/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func weekDatesAndDays()-> ([String], [String]){
        var week: [String] = []
        var weekDays: [String] = []
        for i in 0..<7 {
            let weekDate = Calendar.current.date(byAdding: .weekday, value: -i, to: Date())!
            let dateFormatter = DateFormatter()
            let dayFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dayFormatter.dateFormat = "EE"
            let weekDateString = dateFormatter.string(from: weekDate)
            let weekDayString = dayFormatter.string(from: weekDate)
            week.append(weekDateString)
            weekDays.append(weekDayString)
        }
        weekDays.reverse()
        return (week, weekDays)
    }
    
    func last30DaysDates()-> [String]{
        var month: [String] = []
        for i in 0..<30 {
            let date = Calendar.current.date(byAdding: .weekday, value: -i, to: Date())!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let weekDateString = dateFormatter.string(from: date)
            month.append(weekDateString)
        }
        return month
    }
}

//
//  ThirdViewController.swift
//  MoneyManagerApp
//
//  Created by Mini on 1/16/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import UIKit
import CoreData
import ActionSheetPicker_3_0

class ThirdViewController: BaseViewController, UITextFieldDelegate {

    private var userIsInTheMiddleOfTyping = false
    private var userIsInTheMiddleOfFloatingPointNummer = false
    private var userIsInTheMiddleOfTypingTheFraction = false
    private var userStopTyping = false
    private let decimalSeparator = NumberFormatter().decimalSeparator!
    @IBOutlet weak var customSegmentedControl: CustomSegmentedControl!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var keyOne: UIButton!
    @IBOutlet weak var keyTwo: UIButton!
    @IBOutlet weak var keyThree: UIButton!
    @IBOutlet weak var keyFour: UIButton!
    @IBOutlet weak var keyFive: UIButton!
    @IBOutlet weak var keySix: UIButton!
    @IBOutlet weak var keySeven: UIButton!
    @IBOutlet weak var keyEight: UIButton!
    @IBOutlet weak var keyNine: UIButton!
    @IBOutlet weak var dotKey: UIButton!
    @IBOutlet weak var zeroKey: UIButton!
    @IBOutlet weak var deleteKey: UIButton!
    @IBOutlet weak var okKey: UIButton!
    var incomeItems = [Income]()
    var expenseItems = [Expense]()
    var balanceOverview = [Double]()
    var moc: NSManagedObjectContext!
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let defaults = UserDefaults.standard
    let currencyKeySymbolConstant = "currencyKeySymbol"
    let isFirstLounch = UserDefaults.isFirstLaunch()
    var digits = 0
    var decimalSeparators = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                                 action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        nameTextField.delegate = self
        moc = appDelegate?.persistentContainer.viewContext
        
        display.textColor = #colorLiteral(red: 0, green: 0.9079033732, blue: 0.7564690113, alpha: 1)
        currencyLabel.textColor = #colorLiteral(red: 0, green: 0.9079033732, blue: 0.7564690113, alpha: 1)
        dateTextField.isUserInteractionEnabled = false
        customSegmentedControl.selectedSegmentIndex = 0
        display.text = "0"
        
        if defaults.value(forKeyPath: currencyKeySymbolConstant) == nil {
            currencyLabel.text = "$"
        } else {
            if let currencyCode = defaults.string(forKey: currencyKeySymbolConstant) {
                currencyLabel.text = currencyCode
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        updateViewConstraints()
        addBorderToButtons()
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }

    @IBAction func goBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    private var displayValue: Double {
        get{
            return Double(display.text!)!
        }
        set{
            display.text = String(newValue)
        }
    }
    
    @IBAction func customSegmentedValueChanged(_ sender: CustomSegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            display.textColor = #colorLiteral(red: 0, green: 0.9079033732, blue: 0.7564690113, alpha: 1)
            currencyLabel.textColor = #colorLiteral(red: 0, green: 0.9079033732, blue: 0.7564690113, alpha: 1)
        }
        else if sender.selectedSegmentIndex == 1 {
            display.textColor = #colorLiteral(red: 0.8900626302, green: 0.01348789688, blue: 0, alpha: 1)
            currencyLabel.textColor = #colorLiteral(red: 0.8900626302, green: 0.01348789688, blue: 0, alpha: 1)
        }
    }
    
    @IBAction func datePickerButton(_ sender: UIButton) {
        let picker = ActionSheetDatePicker(title: "Select date",
                                           datePickerMode: .date,
                                           selectedDate: Date(),
                                           doneBlock: { (picker, value, view) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let date = value as! Date
            self.dateTextField.text = dateFormatter.string(from: date)
            
        }, cancel: { (picker) in
            
        }, origin: self.view)
        picker?.maximumDate = NSDate() as Date!
        picker?.show()
    }

    @IBAction func touchDigit(_ sender: UIButton) {
        var digit = sender.currentTitle!
        if sender.currentTitle == display.text && "0" == display.text { //if zero, return
            return
        }
        
        if digit == decimalSeparator {
            if userIsInTheMiddleOfFloatingPointNummer {
                return
            }
            if !userIsInTheMiddleOfTyping{
                digit = "0" + decimalSeparator
            }
            userIsInTheMiddleOfFloatingPointNummer = true
        }
        
        if digits < 8 {
            if userIsInTheMiddleOfTyping {
                
                let textCurrentInDisplay = display.text!
                display.text = textCurrentInDisplay + digit
                digits += 1
            }else {
                display.text = digit
                userIsInTheMiddleOfTyping = true
            }
        } else {
            let myAlert = UIAlertController(title: "",
                                            message: "Enter a smaller value",
                                            preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK",
                                         style: UIAlertActionStyle.cancel,
                                         handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            return
        }

        
    }
    
    @IBAction func deleteDisplayButton(_ sender: UIButton) {
        
        if userIsInTheMiddleOfTyping {
            if var text = display.text {
                if text.last == "." {
                    userIsInTheMiddleOfFloatingPointNummer = false
                }
                if text == "0" {
                    userIsInTheMiddleOfTyping = false
                    return
                }
                
                text.removeLast()
                
                if text.isEmpty {
                    text = "0"
                    userIsInTheMiddleOfTyping = false
                }
                display.text = text
            }
        } else if userIsInTheMiddleOfFloatingPointNummer {
            if var text = display.text {
                text.removeLast()
                if text.isEmpty {
                    text = "0"
                    userIsInTheMiddleOfFloatingPointNummer = false
                }
                display.text = text
            }
        }
        if display.text == "0" {
            return
        }
        digits -= 1
    }
    
    @IBAction func okButton(_ sender: UIButton) {
     
        let name = nameTextField.text!
        let amount = display.text!
        let date = dateTextField.text!

        if(name.isEmpty || amount == "0" || date.isEmpty){
            let myAlert = UIAlertController(title: "Alert",
                                            message: "All fields are required to fill in",
                                            preferredStyle: UIAlertControllerStyle.alert)

            let okAction = UIAlertAction(title: "OK",
                                         style: UIAlertActionStyle.default,
                                         handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            return
        }
        
        if customSegmentedControl.selectedSegmentIndex == 0 {
            insertIncome()
        }
        else if customSegmentedControl.selectedSegmentIndex == 1 {
            insertExpense()
        }
    }
    
     func insertIncome() {
        let incomeItem = Income(context: moc)
        incomeItem.name = nameTextField.text!
        incomeItem.amount = display.text!
        incomeItem.date = dateTextField.text!
        
        appDelegate?.saveContext()
        navigationController?.popToRootViewController(animated: true)

    }
    
    func insertExpense() {
        let expenseItem = Expense(context: moc)
        expenseItem.name = nameTextField.text!
        expenseItem.amount = "-" + display.text!
        expenseItem.date = dateTextField.text!
        
        appDelegate?.saveContext()
        navigationController?.popToRootViewController(animated: true)

    }
    
    func addBorderToButtons(){
        let borderColor: UIColor = .lightGray
        let borderWidth: CGFloat = 0.5
        
        keyOne.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        keyOne.addBorder(side: .Right, color: borderColor, width: borderWidth)
        
        keyTwo.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        keyTwo.addBorder(side: .Right, color: borderColor, width: borderWidth)
        
        keyFour.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        keyFour.addBorder(side: .Right, color: borderColor, width: borderWidth)
        
        keyFive.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        keyFive.addBorder(side: .Right, color: borderColor, width: borderWidth)
        
        keySeven.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        keySeven.addBorder(side: .Right, color: borderColor, width: borderWidth)
        
        keyEight.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        keyEight.addBorder(side: .Right, color: borderColor, width: borderWidth)
        
        keyThree.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        keySix.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        keyNine.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
        
        dotKey.addBorder(side: .Right, color: borderColor, width: borderWidth)
        deleteKey.addBorder(side: .LeftTop, color: borderColor, width: borderWidth)
        okKey.addBorder(side: .LeftBottom, color: borderColor, width: borderWidth)
        deleteKey.addBorder(side: .Bottom, color: borderColor, width: borderWidth)
    }
}

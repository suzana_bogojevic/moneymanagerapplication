//
//  BaseNavigationViewController.swift
//  MoneyManagerApp
//
//  Created by Mini on 2/6/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import UIKit

class BaseNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

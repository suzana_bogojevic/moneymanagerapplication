//
//  ViewController.swift
//  MoneyManagerApp
//
//  Created by Mini on 1/16/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import UIKit
import CoreData
import Charts
import LocalAuthentication

class MainViewController: BaseViewController, UITableViewDataSource, ChartViewDelegate, UIAlertViewDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var incomeValueLabel: UILabel!
    @IBOutlet weak var expenseValueLabel: UILabel!
    @IBOutlet weak var balanceValueLabel: UILabel!
    @IBOutlet weak var monthBalanceLabel: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!

    var incomeItems = [Income]()
    var expenseItems = [Expense]()
    var dataItems = [Data]()
    var moc: NSManagedObjectContext!
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let defaults = UserDefaults.standard
    let currencyKeyCodeConstant = "currencyKeyCode"
    let switchKeyConstant = "switchKeyName"
    let balanceInit = "0"
    var currencyCode = ""
    var shouldHideData = true
    var shouldHideLine = true
    var deleteDatatIndexPath: NSIndexPath? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        moc = appDelegate?.persistentContainer.viewContext
        navigationController?.navigationBar.isHidden = true
        shouldHideData = false
        updateChartData()
    }
    
    override func awakeFromNib() {
        setDefaultsForCurrencyKeyCode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setDefaultsForCurrencyKeyCode()
        tableView.reloadData()
        incomeExpenseWeekOverview()
        monthlyBalance()
        setChart()
    }
    
    func drawChart(yMin: Double, yMax: Double){
        
        let yAxis = lineChartView.leftAxis
        yAxis.setLabelCount(-1, force: true)
        yAxis.axisLineColor = .clear
        yAxis.drawLimitLinesBehindDataEnabled = true
        yAxis.axisMaximum = yMax
        yAxis.axisMinimum = yMin
    
        let xAxis = lineChartView.xAxis
        xAxis.labelPosition = .bottomInside
        xAxis.labelFont = UIFont(name: "Avenir-Black", size: 12.0)!
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.gridColor = .white
        xAxis.setLabelCount(10, force: true)
        xAxis.axisLineColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        xAxis.drawAxisLineEnabled = false
        xAxis.labelTextColor = #colorLiteral(red: 0.8062958717, green: 0.8067459464, blue: 0.8485581875, alpha: 1)
        xAxis.centerAxisLabelsEnabled = true
        xAxis.granularityEnabled = true
        xAxis.granularity = 3
        xAxis.drawLimitLinesBehindDataEnabled = true

        lineChartView.delegate = self
        lineChartView.setViewPortOffsets(left: 0, top: -0.2, right: 0, bottom: 0)
        lineChartView.drawBordersEnabled = false
        lineChartView.borderColor = .clear
        lineChartView.chartDescription?.enabled = false
        lineChartView.backgroundColor = #colorLiteral(red: 0.1399839818, green: 0.1367071867, blue: 0.4915143847, alpha: 1)
        lineChartView.dragEnabled = false
        lineChartView.setScaleEnabled(false)
        lineChartView.maxHighlightDistance = 300
        lineChartView.rightAxis.enabled = false
        lineChartView.legend.enabled = false
        lineChartView.animate(xAxisDuration: 0.5, yAxisDuration: 1.5, easingOption: ChartEasingOption.easeInSine)
    }
    
    func updateChartData() {
       if self.shouldHideData {
           lineChartView.data = nil
           return
       }
        self.setChart()
    }
    
    func setChart(){
        var(_, xDays) = weekDatesAndDays()
        xDays.insert("  ", at: 0) //empty labels for space left and right
        xDays.append("  ")
        
        var yValues = balanceWeekOverview()
        if !yValues.isEmpty{
            let first = yValues.first!
            let last = yValues.last!
            yValues.insert(first, at: 0)
            yValues.append(last)
        }
        
        lineChartView.setLineChartData(xValues: xDays, yValues: yValues, label: "", show: true)
        
        var minimum = yValues.min()!
        var maximum = yValues.max()!
        if maximum == 0 {
            maximum += 500
        }
        if minimum == 0 {
            minimum -= 500
        }
        
        drawChart(yMin: minimum - (maximum*50)/100, yMax: maximum + (maximum*80)/100)
    }
    
    @IBAction func addNewButton(_ sender: CustomAddNewButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let thirdViewController = storyBoard.instantiateViewController(withIdentifier: "ThirdVC") as? ThirdViewController
        {
            navigationController?.pushViewController(thirdViewController, animated: true)
        }
    }
    
    @IBAction func showSecondVC(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let secondViewController = storyBoard.instantiateViewController(withIdentifier: "SecondVC") as? DrawerViewController
        {
            self.present(secondViewController, animated: true, completion: nil)
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteDatatIndexPath = indexPath as NSIndexPath
            let dataToDelete = dataItems[indexPath.row]
            confirmDelete(data: dataToDelete)
        }
    }
    
    func confirmDelete(data: Data) {
        let alert = UIAlertController(title: "Delete Data", message: "Are you sure you want to permanently delete \(data.name!)?", preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: handleDeleteData)
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: cancelDeleteData)
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)

        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleDeleteData(alertAction: UIAlertAction!) -> Void {
        if let indexPath = deleteDatatIndexPath {
            tableView.beginUpdates()
            
            moc.delete(dataItems[indexPath.row])
            dataItems.remove(at: indexPath.row)
            do{
                try moc.save()
            } catch let error as NSError { print("Error while deleting: \(error.userInfo)")}
            
            tableView.deleteRows(at: [indexPath as IndexPath], with: .automatic)
            
            deleteDatatIndexPath = nil
            incomeExpenseWeekOverview()
            monthlyBalance()
            setChart()
            tableView.endUpdates()
        }
    }
    
    func cancelDeleteData(alertAction: UIAlertAction!) {
        deleteDatatIndexPath = nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        
        let dataItem = dataItems[indexPath.row]
        let dataName = dataItem.name
        var dataAmount = dataItem.amount!
        if dataAmount.hasPrefix("-"){
            dataAmount.remove(at: dataAmount.startIndex)
        }
        
        let dataDate = dataItem.date!
        cell.nameCell.text = dataName
        cell.dateCell.text = dataDate
        
        if let amount = Double(dataAmount) {
            cell.amountCell.text = amount.formatAsCurrency(currencyCode: currencyCode)!
        }
        
        if let _ = dataItem as? Income {
            cell.amountCell.textColor = #colorLiteral(red: 0, green: 0.9079033732, blue: 0.7564690113, alpha: 1)
            
        } else if let _ = dataItem as? Expense {
            cell.amountCell.textColor = #colorLiteral(red: 0.8900626302, green: 0.01348789688, blue: 0, alpha: 1)
        }
        
        cell.backgroundColor = (indexPath.row % 2 == 0 ? #colorLiteral(red: 0.9486700892, green: 0.9493889213, blue: 0.9487814307, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
       // cell.amountCell.adjustsFontSizeToFitWidth = true
        return cell
    }
    
    func setDefaultsForCurrencyKeyCode(){
        if defaults.value(forKeyPath: currencyKeyCodeConstant) == nil {
            currencyCode = "USD"
        } else {
            if let currencyValue = defaults.string(forKey: currencyKeyCodeConstant) {
                currencyCode = currencyValue
            }
        }
    }
    
    func balanceWeekOverview() -> [Double]{
        var balanceOverviewValues = [Double]()
        var balanceSumOverDate = 0.0
        let(weekArray, _) = weekDatesAndDays();
        let data = loadData()
        let dates = (data.filter({!$0.date!.isEmpty}).map({$0.date!}))
        var unique = Array(Set(dates)).sorted()
        
        // if there is no data entered on specific days, add that day to array of dates and continue to calculate
        for day in weekArray {
            if !unique.contains(day){
                unique.append(day)
            }
        }

        for date in unique.sorted() {
            if weekArray.contains(date){
                balanceSumOverDate = data.filter({$0.date! == date}).map({Double($0.amount!)!}).reduce(0.0, +)
                balanceOverviewValues.append(balanceSumOverDate)
            }
        }
        
        let totalBalanceForSevenDays = balanceOverviewValues.reduce(0, +)
        balanceLastSevenDays(balance: totalBalanceForSevenDays)
        return balanceOverviewValues
    }
    
    func incomeExpenseWeekOverview(){
        var incomeOverview = [Double]()
        var expenseOverview = [Double]()
        var incomeSumOverDate = 0.0
        var expenseSumOverDate = 0.0
        let data = loadData()
        let income = loadIncome()
        let expense = loadExpense()
        let dates = (data.filter({!$0.date!.isEmpty}).map({$0.date!}))
        var unique = Array(Set(dates)).sorted()
        let(weekArray, _) = weekDatesAndDays();
        
        // if there is no data entered on specific days, add that day to array of dates and continue to calculate
        for day in weekArray {
            if !unique.contains(day){
                unique.append(day)
            }
        }
        
        for date in unique.sorted() {
            if weekArray.contains(date){
                incomeSumOverDate = income.filter({$0.date! == date}).map({Double($0.amount!)!}).reduce(0.0, +)
                expenseSumOverDate = expense.filter({$0.date! == date}).map({Double($0.amount!)!}).reduce(0.0, +)
                incomeOverview.append(incomeSumOverDate)
                expenseOverview.append(expenseSumOverDate)
            }
        }
        let totalIncomeForSevenDays = incomeOverview.reduce(0, +)
        let totalExpenseForSevenDays = abs(expenseOverview.reduce(0, +))
        totalIncomeLastSevenDays(sum: totalIncomeForSevenDays)
        totalExpenseLastSevenDays(sum: totalExpenseForSevenDays)
    }
    
    func monthlyBalance() {
        var monthArrayDates: [String] = []
        var balanceOverview : [Double] = []
        var balanceSum = 0.0
        let data = loadData()
        monthArrayDates = last30DaysDates()
        
        let dates = (data.filter({!$0.date!.isEmpty}).map({$0.date!}))
        let unique = Array(Set(dates)).sorted()
        
        for date in unique {
            if monthArrayDates.contains(date){
                balanceSum = data.filter({$0.date! == date}).map({Double($0.amount!)!}).reduce(0.0, +)
                balanceOverview.append(balanceSum)
            }
        }
        
        let balanceOverMonth = balanceOverview.reduce(0, +)
        monthBalanceLabel.text = (balanceOverMonth == 0.0 ? balanceInit : balanceOverMonth.formatAsCurrency(currencyCode: currencyCode))
       // monthBalanceLabel.adjustsFontSizeToFitWidth = true

    }
    
    func totalIncomeLastSevenDays(sum: Double){
        incomeValueLabel.text = sum.formatAsCurrency(currencyCode: currencyCode)
       // incomeValueLabel.adjustsFontSizeToFitWidth = true
    }
    
    func totalExpenseLastSevenDays(sum: Double){
        expenseValueLabel.text = sum.formatAsCurrency(currencyCode: currencyCode)
       // expenseValueLabel.adjustsFontSizeToFitWidth = true
    }
    
    func balanceLastSevenDays(balance: Double){
        if let currencyValue = defaults.string(forKey: currencyKeyCodeConstant) {
            currencyCode = currencyValue
        }
        balanceValueLabel.text = (balance == 0.0 ? balanceInit : balance.formatAsCurrency(currencyCode: currencyCode))
       // balanceValueLabel.adjustsFontSizeToFitWidth = true
    }
    
    // CORE DATA handling
    func loadData() -> [Data]{
        let dataRequest: NSFetchRequest<Data> = Data.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        dataRequest.sortDescriptors = [sortDescriptor]
        
        do {
            try dataItems = moc.fetch(dataRequest)
        }catch {
            print("Something went wrong while loading Data")
        }
       
        self.tableView.reloadData()
        return dataItems
    }
    
    func loadIncome() -> [Income] {
        let incomeRequest: NSFetchRequest<Income> = Income.fetchRequest()
        do {
            try incomeItems = moc.fetch(incomeRequest)
        }catch {
            print("Something went wrong while loading Income")
        }
        return incomeItems
    }
    
    func loadExpense() -> [Expense] {
        let expenseRequest: NSFetchRequest<Expense> = Expense.fetchRequest()
        do {
            try expenseItems = moc.fetch(expenseRequest)
        }catch {
            print("Something went wrong while loading Expense")
        }
        return expenseItems
    }

    func batchDelete() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Data")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
       
        do {
            try moc.execute(batchDeleteRequest)
       
        } catch {
            // Error Handling
        }
    }
}

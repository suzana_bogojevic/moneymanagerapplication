//
//  CustomTableViewCell.swift
//  MoneyManagerApp
//
//  Created by Mini on 1/22/18.
//  Copyright © 2018 Mini. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var dateCell: UILabel!
    @IBOutlet weak var amountCell: UILabel! 
}
